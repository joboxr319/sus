# class Player(object):
#     # if given a dbid, set class attributes from database value
#     def __init__(self, dbid=None):
#         _table = self.__class__.__name__
#         self.fields = db[_table].fields
#         for f in self.fields:
#             setattr(self, f, None)
#         if dbid:
#             row = db(db[_table].id == dbid).select().first().as_dict()
#             self.dbid = dbid
#             for key, value in row.iteritems():
#                 setattr(self, key, value)
#         else:
#             # create db record and get name and id
#             self.dbid = db[_table].insert()
#
#     def save(self):
#         _table = self.__class__.__name__
#         for key, value in vars(self).iteritems():
#             if key not in ['dbid', 'fields', 'id']:
#                 db(db[_table]._id == self.dbid).update(**{key:value})
#                 print key, value
#
# class Team(object):
#     # if given a dbid, set class attributes from database value
#     def __init__(self, dbid=None):
#         _table = self.__class__.__name__
#         self.fields = db[_table].fields
#         for f in self.fields:
#             setattr(self, f, None)
#         if dbid:
#             row = db(db[_table].id == dbid).select().first().as_dict()
#             self.dbid = dbid
#             for key, value in row.iteritems():
#                 setattr(self, key, value)
#
#             # handle many to many membership
#             self.members = []
#             _members = db(db.Team_Player.team == dbid).select(db.Team_Player.Player)
#             if _members:
#                 for player in _members:
#                     player_id = player.Player
#                     print player_id
#                     self.members.append(Player(player_id))
#         else:
#             # create db record and get id
#             self.dbid = db[_table].insert()
#             self.members = []
#
#     def save(self):
#         _table = self.__class__.__name__
#         for key, value in vars(self).iteritems():
#             if key not in ['dbid', 'fields', 'id']:
#                 db(db[_table]._id == self.dbid).update(**{key:value})
#                 print key, value
#
#     def add_member(self, member):
#         self.members.append(member)
#         player_id = member.dbid
#         print player_id
#         db.Team_Player.insert(Team=self.dbid, Player=player_id)
#
#     def stats(self):
#         if self.wins > 0:
#             self.win_percent = self.wins / float(self.wins + self.losses)
#             self.point_diff = self.points_for - self.points_against
#             self.save()

# class Game(object):
#     def __init__(self, home, away):
#         self.tourney_id = 0
#         self.home = home
#         self.away = away
#         self.results = {self.home: 0, self.away: 0}
#         self.finished = False
#         self.winner = None
#         self.loser = None
#
#     def update_score(self, team, score):
#         self.results[team] = score;
#         self.home.save()
#         self.away.save()
#
#     def finish(self):
#         self.finished = True
#         self.winner = max(self.results, key=self.results.get)
#         self.loser = min(self.results, key=self.results.get)
#         self.winner.wins += 1
#         self.winner.points_for += self.results[self.winner]
#         self.winner.points_against += self.results[self.loser]
#         self.loser.losses += 1
#         self.loser.points_for += self.results[self.loser]
#         self.loser.points_against += self.results[self.winner]
#         self.winner.stats()
#         self.loser.stats()
#         self.winner.save()
#         self.loser.save()

# class Game(object):
#
#     # if given a dbid, set class attributes from database value
#     def __init__(self, dbid=None, home=None, away=None):
#         _table = self.__class__.__name__
#         self.fields = db[_table].fields
#         for f in self.fields:
#             setattr(self, f, None)
#
#         if dbid:
#             row = db(db[_table].id == dbid).select().first().as_dict()
#             self.dbid = dbid
#             for key, value in row.iteritems():
#                 setattr(self, key, value)
#
#             # get home and away teams from database
#             teams = db(db.Game_Team.game == dbid).select().first()
#             for t in teams:
#                 self.home = Team(t.home)
#                 self.away = Team(t.away)
#
#         else:
#             # create db record and get id
#             self.dbid = db[_table].insert()
#             self.home = home
#             self.away = away
#
#     def save(self):
#         _table = self.__class__.__name__
#         for key, value in vars(self).iteritems():
#             if key not in ['dbid', 'fields', 'id', 'home', 'away']:
#                 db(db[_table]._id == self.dbid).update(**{key:value})
#                 print key, value
#
#     def update_score(self, team, score):
#         if team == self.home:
#             self.home_score += score
#         if team == self.away:
#             self.away_score += score
#         self.save()
#
#     def finish(self):
#         self.finished = True
#         self.save()
#
#         # finalize stats and assign to teams
#         self.winner = None
#         self.loser = None
#
#         if self.home_score >= self.away_score:
#             self.winner = self.home
#             self.winner.score = self.home_score
#             self.loser = self.away
#             self.loser.score = self.away_score
#         else:
#             self.winner = self.away
#             self.winner.score = self.away_score
#             self.loser = self.home
#             self.loser = self.home_score
#
#         self.winner.wins += 1
#         self.winner.points_for += self.winner.score
#         self.winner.points_against += self.loser.score
#         self.loser.losses += 1
#         self.loser.points_for += self.loser.score
#         self.loser.points_against += self.winner.score
#         self.winner.stats()
#         self.loser.stats()
#         self.winner.save()
#         self.loser.save()
