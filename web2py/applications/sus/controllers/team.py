def index():
    # display create team
    # or join team

    # expect a tournament id
    if not request.args(0):
        redirect(URL('default', 'index'))
    else:
        tourney = Tournament(request.args(0))

    return dict()

@auth.requires_login()
def create():

    tourney = None
    player = None
    team = None

    # expect a tournament id
    if not request.args(0):
        redirect(URL('default', 'index'))
    else:
        tourney = Tournament(request.args(0))

    player = register_player()

    # TODO: move stats to own class
    db.Team.wins.readable = db.Team.wins.writable = False
    db.Team.losses.readable = db.Team.losses.writable = False
    db.Team.points_for.readable = db.Team.points_for.writable = False
    db.Team.points_against.readable = db.Team.points_against.writable = False
    db.Team.win_percent.readable = db.Team.win_percent.writable = False
    db.Team.point_diff.readable = db.Team.point_diff.writable = False
    db.Team.score.readable = db.Team.score.writable = False

    form = SQLFORM(db.Team, formstyle='bootstrap', _class='')
    form.custom.widget.name['_class'] = 'form-control string'
    form.custom.widget.password['_class'] = 'form-control password'
    form.custom.widget.password['_autocomplete'] = 'False'
    if form.process().accepted:
        team = Team(form.vars.id)
        team.add_member(player)
        team.save()
        tourney.add_team(team)
        redirect(URL('team', 'view', args=form.vars.id))

    return dict(form=form)

@auth.requires_login()
def join():
    # action when requesting to join team
    # expecting a team id
    if not request.args(0):
        redirect(URL('default', 'index'))
    else:
        team = Team(request.args(0))

    player = register_player()

    # if player is already registered to team, redirect to view
    member_ids = []
    for member in team.members:
        member_ids.append(member.dbid)
    if player.dbid in member_ids:
        redirect(URL('team', 'view', args=team.dbid, extension='html'))

    # if team has password, require password before allowing join
    if team.password:
        form = SQLFORM.factory(Field('password'))
        if form.process().accepted:
            if form.vars.password == team.password:
                team.add_member(player)
                team.save()
                redirect(URL('team', 'view', args=team.dbid, extension='html'))
            else:
                response.flash = 'password incorrect!'
    else:
        form = None
        team.add_member(player)
        team.save()
        redirect(URL('team', 'view', args=team.dbid))

    return dict(form=form)

def view():
    if not request.args(0):
        redirect(URL('default', 'index'))

    # team = db(db.Team.id == request.args(0)).select().first()
    team = Team(request.args(0))

    members = []
    for member in team.members:
        members.append(member.name)

    tournament = db(db.Tournament_Team.team == team.dbid).select().first()
    tournament_id = tournament.tournament
    tourney = Tournament(tournament_id)

    return dict(name=team.name, members=members, tourney=tourney)